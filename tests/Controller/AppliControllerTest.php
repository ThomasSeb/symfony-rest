<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use App\DataFixtures\AppliFixtures;
use App\DataFixtures\AppFixtures;
use Doctrine\ORM\Tools\SchemaTool;

class AppliControllerTest extends WebTestCase
{

    public function setUp()
    {
        self::bootKernel();
        $manager = self::$container->get('doctrine.orm.entity_manager');
        // $schemaTool = new SchemaTool($manager);
        // $classes = $manager->getMetadataFactory()->getAllMetadata();
        // $schemaTool->dropSchema($classes);
        // $schemaTool->createSchema($classes);
        $purger = new ORMPurger($manager);
        // $purger->setPurgeMode(ORMPurger::PURGE_MODE_TRUNCATE);
        $purger->purge();
        $manager->getConnection()->exec("ALTER TABLE appli AUTO_INCREMENT = 1;");
        $fixture = new AppliFixtures();
        $fixture->load($manager);
    }

    public function testGetAll()
    {
        $client = static::createClient();
        $client->request('GET', '/appli');

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(5, $data);
        $this->assertSame('appli 1', $data[0]['name']);
    }

    public function testAddSuccess()
    {
       $client = static::createClient();
       $client->request('POST', '/appli', [], [], [], json_encode([
           "name" => "applitest",
           "size" => 5,
           "lastUpdate" => '2019-04-08',
           "downloads" =>2345
       ]));

       $this->assertSame(201, $client->getResponse()->getStatusCode());
       $repo = self::$container->get('App\Repository\AppliRepository');
       $this->assertSame(6, $repo->count([]));

    }    

    public function testGetOne()
    {
       $client = static::createClient();
       $client->request('GET', '/appli/1');

       $this->assertSame(200, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertSame('appli 1', $data['name']);

    }

    public function testDelete()
    {
        $client = static::createClient();
        $client->request('DELETE', '/appli/1');
        $this->assertSame(204, $client->getResponse()->getStatusCode());

        $repo = self::$container->get('App\Repository\AppliRepository');
        $this->assertSame(4, $repo->count([]));
    }

    public function testUpdateSuccess() {

        $client = static::createClient();
        $client->request('PATCH', '/appli/2', [], [], [], json_encode([
            "name" => "test patch"
        ]));

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent(), true);        
        $this->assertSame('test patch', $data['name']);
    }
}
