<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\DataFixtures\AppFixtures;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\Tools\SchemaTool;

class DeviceControllerTest extends WebTestCase
{

    public function setUp() {
        
        self::bootKernel();
        $manager = self::$container->get('doctrine.orm.entity_manager');
        // $schemaTool = new SchemaTool($manager);
        // $classes = $manager->getMetadataFactory()->getAllMetadata();
        // $schemaTool->dropSchema($classes);
        // $schemaTool->createSchema($classes);
        $purger = new ORMPurger($manager);
        // $purger->setPurgeMode(ORMPurger::PURGE_MODE_TRUNCATE);
        $purger->purge();
        $manager->getConnection()->exec("ALTER TABLE device AUTO_INCREMENT = 1;");
        $fixture = new AppFixtures();
        $fixture->load($manager);
    }
    public function testGetAll()
    {
       $client = static::createClient();
        $client->request('GET', '/device');

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(5, $data);
        $this->assertSame('Device 1', $data[0]['label']);
        
    }

    public function testAddSuccess() {

        $client = static::createClient();
        $client->request('POST', '/device', [], [], [], json_encode([
            "label" => "test",
            "ip" => "192.168.1.1",
            "os" => "Android",
            "battery" => 35
        ]));

        $this->assertSame(201, $client->getResponse()->getStatusCode());
        $repo = self::$container->get('App\Repository\DeviceRepository');
        $this->assertSame(6, $repo->count([]));
    }

    public function testGetOne() {
        $client = static::createClient();
        $client->request('GET', '/device/1');
        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertSame(1, $data['id']);



    }

    public function testDelete() {
        $client = static::createClient();
        $client->request('DELETE', '/device/1');
        $this->assertSame(204, $client->getResponse()->getStatusCode());

        $repo = self::$container->get('App\Repository\DeviceRepository');
        $this->assertSame(4, $repo->count([]));
    }

    public function testUpdateSuccess() {

        $client = static::createClient();
        $client->request('PATCH', '/device/2', [], [], [], json_encode([
            "label" => "test patch"
        ]));

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent(), true);        
        $this->assertSame('test patch', $data['label']);
    }
}
