<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Device;
use App\Entity\Appli;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        for ($i=1; $i <= 5; $i++) { 
            $device = new Device();

            $device->setLabel('Device ' . $i);
            $device->setIp('192.168.0.'.$i);
            $device->setOs('OS ' . $i);
            $device->setBattery(10*$i);
            $manager->persist($device);

        }

        // for ($x=1; $x <= 5; $x++) { 
        //     $appli = new Appli();

        //     $appli->setName('appli ' . $x);
        //     $appli->setSize(10*$x);
        //     $appli->setDownloads(100*$x);
            
        //     $manager->persist($appli);

        // }

        $manager->flush();
    }
}
