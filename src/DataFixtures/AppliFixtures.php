<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Appli;

class AppliFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        for ($i=1; $i <= 5; $i++) { 
            $appli = new Appli();

            $appli->setName('appli ' . $i);
            $appli->setSize(10*$i);
            $appli->setLastUpdate(new \DateTime());
            $appli->setDownloads(100*$i);
            
            $manager->persist($appli);

        }

        $manager->flush();
    }
}