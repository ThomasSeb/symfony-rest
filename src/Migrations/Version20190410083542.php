<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190410083542 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE appli (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, size INT NOT NULL, last_update DATETIME NOT NULL, downloads INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE appli_device (appli_id INT NOT NULL, device_id INT NOT NULL, INDEX IDX_D8CA76301DC59C41 (appli_id), INDEX IDX_D8CA763094A4C7D4 (device_id), PRIMARY KEY(appli_id, device_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE appli_device ADD CONSTRAINT FK_D8CA76301DC59C41 FOREIGN KEY (appli_id) REFERENCES appli (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE appli_device ADD CONSTRAINT FK_D8CA763094A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE appli_device DROP FOREIGN KEY FK_D8CA76301DC59C41');
        $this->addSql('DROP TABLE appli');
        $this->addSql('DROP TABLE appli_device');
    }
}
