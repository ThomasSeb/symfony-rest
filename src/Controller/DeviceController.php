<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use JMS\Serializer\SerializerInterface;
use App\Repository\DeviceRepository;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Device;
use Symfony\Component\HttpFoundation\Request;
use App\Form\DeviceType;

/**
 * @Route("/device", name="device")
 */
class DeviceController extends AbstractController
{

    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route(methods="GET")
     */
    public function index(DeviceRepository $repo)
    {
        $devices = $repo->findAll();

        // return new Response(json_encode(["ga"=>"bloup"]));
        // return $this->json(["ga"=>"bloup"]);

        return new JsonResponse($this->serializer->serialize($devices, 'json'), 200, [], true);
    }

    /**
     * @Route(methods="POST")
     */
    public function add(ObjectManager $manager, Request $request)
    {

        $device = new Device();

        $form = $this->createForm(DeviceType::class, $device);
        $form->submit(json_decode($request->getContent(), true));

        if ($form->isSubmitted() && $form->isValid()) {

            $manager->persist($device);
            $manager->flush();

            return new JsonResponse($this->serializer->serialize($device, 'json'), 201, [], true);
        }
        return $this->json($form->getErrors(true), 400);
    }

    /**
     * @Route("/{device}", methods="GET")
     */
    public function getOne(Device $device)
    {

        return new JsonResponse($this->serializer->serialize($device, 'json'), 200, [], true);
    }

    /**
     * @Route("/{device}", methods="DELETE")
     */
    public function delete(Device $device, ObjectManager $manager)
    {
        $manager->remove($device);
        $manager->flush();

        return $this->json('No content', 204);
    }

    /**
     * @Route("/{device}", methods="PATCH")
     */
    public function update(Device $device, ObjectManager $manager, Request $request)
    {

        $form = $this->createForm(DeviceType::class, $device);
        $form->submit(json_decode($request->getContent(), true),false);

        if ($form->isSubmitted() && $form->isValid()) {

            $manager->flush();

            return new JsonResponse($this->serializer->serialize($device, 'json'), 200, [], true);
        }
        return $this->json($form->getErrors(true), 400);

    }

}
