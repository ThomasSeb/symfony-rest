<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use JMS\Serializer\SerializerInterface;
use App\Repository\AppliRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Appli;
use App\Form\AppliType;

/**
 * @Route("/appli", name="appli")
 */
class AppliController extends AbstractController
{

    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }
    
    /**
     * @Route(methods="GET")
     */
    public function index(AppliRepository $repo)
    {
        $applis = $repo->findAll();

        return new JsonResponse($this->serializer->serialize($applis, 'json'), JsonResponse::HTTP_OK, [], true);
    }

    /**
     * @Route(methods="POST")
     */
    public function add(ObjectManager $manager, Request $request){

        $appli = new Appli();

        $form = $this->createForm(AppliType::class, $appli);
        $form->submit(json_decode($request->getContent(), true));

        if ($form->isSubmitted() && $form->isValid()) {
           $manager->persist($appli);
           $manager->flush();

           return new JsonResponse($this->serializer->serialize($appli, 'json'), JsonResponse::HTTP_CREATED, [], true);
        }
        return $this->json($form->getErrors(true), JsonResponse::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/{appli}", methods="GET")
     */
    public function getOne(Appli $appli) {

        return new JsonResponse($this->serializer->serialize($appli, 'json'), JsonResponse::HTTP_OK, [], true);
    }

    /**
     * @Route("/{appli}", methods="DELETE")
     */
    public function remove(Appli $appli, ObjectManager $manager){

        $manager->remove($appli);
        $manager->flush();

        return $this->json('', JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/{appli}", methods="PATCH")
     */
    public function update(Appli $appli, ObjectManager $manager, Request $request) {

        $form = $this->createForm(AppliType::class, $appli);
        $form->submit(json_decode($request->getContent(), true), false);

        if ($form->isSubmitted() && $form->isValid()) {

            $manager->flush();
 
            return new JsonResponse($this->serializer->serialize($appli, 'json'), JsonResponse::HTTP_OK, [], true);
         }
         return $this->json($form->getErrors(true), JsonResponse::HTTP_BAD_REQUEST);
    }
}
